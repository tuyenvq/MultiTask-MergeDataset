import numpy as np
import os
import cv2
import scipy
import random

SMILE_FOLDER = '../data/MultiTask/smile_data/'
EMOTION_FOLDER = '../data/MultiTask/emotion_data/'
GENDER_FOLDER = '../data/MultiTask/gender_data/'
IMDB_FOLDER = '../data/MultiTask/imdb_data/'
NUM_SMILE_IMAGE = 4000
SMILE_SIZE = 48
EMOTION_SIZE = 48


def getSmileImage():
    print('Load smile image...................')
    X1 = np.load(SMILE_FOLDER + 'train.npy')
    X2 = np.load(SMILE_FOLDER + 'test.npy')

    train_data = []
    test_data = []
    for i in range(X1.shape[0]):
        train_data.append(X1[i])
    for i in range(X2.shape[0]):
        test_data.append(X2[i])

    print('Done !')
    print('Number of smile train data: ',str(len(train_data)))
    print('---------------------------------------------------------------')
    return train_data, test_data


def getGenderImage():
    print('Load gender image...................')
    X1 = np.load(GENDER_FOLDER + 'train.npy')
    X2 = np.load(GENDER_FOLDER + 'test.npy')

    train_data = []
    test_data = []
    for i in range(X1.shape[0]):
        train_data.append(X1[i])
    for i in range(X2.shape[0]):
        test_data.append(X2[i])

    print('Done !')
    print('Number of gender train data: ', str(len(train_data)))
    print('---------------------------------------------------------------')
    return train_data, test_data

def getImdbImage():
    print('Load gender image...................')
    X1 = np.load(IMDB_FOLDER + 'train.npy')
    X2 = np.load(IMDB_FOLDER + 'test.npy')

    train_data = []
    test_data = []
    for i in range(X1.shape[0]):
        train_data.append(X1[i])
    for i in range(X2.shape[0]):
        test_data.append(X2[i])

    print('Done !')
    print('Number of gender train data: ', str(len(train_data)))
    print('---------------------------------------------------------------')
    return train_data, test_data


def getEmotionImage():
    print('Load emotion image..................')
    train_images, train_labels, validation_images, validation_labels = [], [], [], []
    if os.path.isfile(EMOTION_FOLDER + 'temp_train_and_validation/train/images.npy'):
        train_images = np.load(EMOTION_FOLDER + 'temp_train_and_validation/train/images.npy')
        train_labels = np.load(EMOTION_FOLDER + 'temp_train_and_validation/train/labels.npy')
        validation_images = np.load(EMOTION_FOLDER + 'temp_train_and_validation/validation/images.npy')
        validation_labels = np.load(EMOTION_FOLDER + 'temp_train_and_validation/validation/labels.npy')
    train_data = []
    for i in range(len(train_images)):
        T = train_images[i]
        label = train_labels[i]
        train_data.append((T, label))

    for i in range(len(validation_images)):
        T = validation_images[i]
        label = validation_labels[i]
        train_data.append((T, label))
    np.random.shuffle(train_data)

    public_test_images = np.load(EMOTION_FOLDER + 'public test/images.npy')
    public_test_labels = np.load(EMOTION_FOLDER + 'public test/labels.npy')
    private_test_images = np.load(EMOTION_FOLDER + 'private test/images.npy')
    private_test_labels = np.load(EMOTION_FOLDER + 'private test/labels.npy')

    public_test_data = []
    private_test_data = []
    for i in range(len(public_test_images)):
        T = public_test_images[i]
        label = public_test_labels[i]
        public_test_data.append((T, label))

    for i in range(len(private_test_images)):
        T = private_test_images[i]
        label = private_test_labels[i]
        private_test_data.append((T, label))

    print('Done !')
    print('Number of emotion train data: ', str(len(train_data)))
    print('---------------------------------------------------------------')
    return train_data, public_test_data, private_test_data


''' Data augmentation method '''


def random_crop(batch, crop_shape, padding=None):
    oshape = np.shape(batch[0])
    if padding:
        oshape = (oshape[0] + 2 * padding, oshape[1] + 2 * padding)
    new_batch = []
    npad = ((padding, padding), (padding, padding), (0, 0))
    for i in range(len(batch)):
        new_batch.append(batch[i])
        if padding:
            new_batch[i] = np.lib.pad(batch[i], pad_width=npad, mode='constant', constant_values=0)
        nh = random.randint(0, oshape[0] - crop_shape[0])
        nw = random.randint(0, oshape[1] - crop_shape[1])
        new_batch[i] = new_batch[i][nh:nh + crop_shape[0], nw:nw + crop_shape[1]]
    return new_batch


def random_flip_leftright(batch):
    for i in range(len(batch)):
        if bool(random.getrandbits(1)):
            batch[i] = np.fliplr(batch[i])
    return batch


def random_flip_updown(batch):
    for i in range(len(batch)):
        if bool(random.getrandbits(1)):
            batch[i] = np.flipud(batch[i])
    return batch


def random_90degrees_rotation(batch, rotations=[0, 1, 2, 3]):
    for i in range(len(batch)):
        num_rotations = random.choice(rotations)
        batch[i] = np.rot90(batch[i], num_rotations)
    return batch


def random_rotation(batch, max_angle):
    for i in range(len(batch)):
        if bool(random.getrandbits(1)):
            angle = random.uniform(-max_angle, max_angle)
            batch[i] = scipy.ndimage.interpolation.rotate(batch[i], angle, reshape=False)
    return batch


def random_blur(batch, sigma_max=5.0):
    for i in range(len(batch)):
        if bool(random.getrandbits(1)):
            sigma = random.uniform(0., sigma_max)
            batch[i] = scipy.ndimage.filters.gaussian_filter(batch[i], sigma)
    return batch


def augmentation(batch, img_size):
    batch = random_crop(batch, (img_size, img_size), 10)
    #batch = random_blur(batch)
    batch = random_flip_leftright(batch)
    batch = random_rotation(batch, 10)

    return batch
